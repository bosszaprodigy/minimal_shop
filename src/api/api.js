import axios from 'axios';

const funcs = {
  getData() {
    return new Promise((resolve, reject) => {
      axios.get("https://dummyjson.com/products?limit=3", {})
        .then(function(response) {
          resolve(response.data);
        })
        .catch(function(error) {
          reject(error);
        });
    });
  },

}

export default funcs